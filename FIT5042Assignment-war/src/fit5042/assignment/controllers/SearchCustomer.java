package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import fit5042.assignment.controllers.*;
import fit5042.assignment.repository.entities.Customer;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private Customer customer;
	AUSApplication app;

	private int searchByInt;
	
	public SearchCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "ausApplication");

		app.updateCustomerList();
	}
	public void searchByCustomerId(int customerId) {
		try {
            //search this property then refresh the list in PropertyApplication bean
            app.searchByCustomerId(customerId);
        } catch (Exception ex) {

        }
	}
	
	
	public Customer getCustomer() {
		return customer;
	}

	
	public int getSearchByInt() {
		return searchByInt;
	}


	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}


	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	
	public AUSApplication getApp() {
		return app;
	}

	
	public void setApp(AUSApplication app) {
		this.app = app;
	}
	
    public void searchAll() {
        try {
            //return all properties from db via EJB
            app.searchAll();
        } catch (Exception ex) {

        }
        //showForm = true;
    }

    public void customerToIndex(int ID) {
    	fit5042.assignment.controllers.CustomerController customer1 = new CustomerController();
    	searchByInt = ID;
    	customer1.setCustomerIndex(ID);
    }
}
