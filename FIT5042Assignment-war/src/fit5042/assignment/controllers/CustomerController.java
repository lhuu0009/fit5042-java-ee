package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.Customer;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {
	
	CustomerManagedBean customerManagedBean; 
	Customer customerTemp;
	AUSApplication app;
	private int customerIndex;
	private Customer customer;

	public CustomerController() {
		// Assign customer identifier via GET param
		// this customerindex is the index, don't confuse with the customer Id
		
		customerIndex = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerIndex"));
		customer = getCustomer();
	}

	/**
	 * @return the customerIndex
	 */
	public int getCustomerIndex() {
		return customerIndex;
	}

	/**
	 * @param customerIndex the customerIndex to set
	 */
	public void setCustomerIndex(int customerIndex) {
		this.customerIndex = customerIndex;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		 if (customer == null) {
	            // Get application context bean PropertyApplication 
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            AUSApplication app
	                    = (AUSApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "ausApplication");
	            // -1 to customerIndex since we +1 in JSF (to always have positive property id!) 
	            return app.getCustomers().get(--customerIndex); //this customerIndex is the index, don't confuse with the Customer Id
	        }
	        return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public void addCustomer(Customer customer) {
		try {
			customerManagedBean.addCustomer(customer);
			app.searchAll();
		}catch (Exception ex) {}
	}

	
}
