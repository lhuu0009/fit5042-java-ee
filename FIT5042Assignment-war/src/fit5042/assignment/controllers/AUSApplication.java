package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.ContactPersonManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;

@Named(value = "ausApplication")
@ApplicationScoped
public class AUSApplication {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    @ManagedProperty(value = "#{contactPersonManagedBean}")
    ContactPersonManagedBean contactPersonManagedBean;
    
    private ArrayList<Customer> customers;
    private ArrayList<ContactPerson> contacts;
    private ShowCustomer showCustomer;
    private ShowContacts showContacts;
    // Add some customer data from db to app 
    public AUSApplication() throws Exception {
    	customers = new ArrayList<Customer>();
    	contacts = new ArrayList<ContactPerson>();
    	showCustomer = new ShowCustomer();
    	showContacts = new ShowContacts();
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
        contactPersonManagedBean = (ContactPersonManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "contactPersonManagedBean");

        //get properties from db 
        updateCustomerList();
        updateContactPersonList();
    }
    
    public ShowCustomer getShowCustomer() {
		return showCustomer;
	}

	public void setShowCustomer(ShowCustomer showCustomer) {
		this.showCustomer = showCustomer;
	}
	

	public ShowContacts getShowContacts() {
		return showContacts;
	}

	public void setShowContacts(ShowContacts showContacts) {
		this.showContacts = showContacts;
	}

	public void updateCustomerList() {
        	customers.clear();

            for (Customer customer : customerManagedBean.getAllCustomers())
            {
            	customers.add(customer);
                }

            setCustomers(customers);
    }
    
    public void updateContactPersonList() {
    	contacts.clear();

        for (ContactPerson contactPerson : customerManagedBean.getAllContactPerson())
        {
        	contacts.add(contactPerson);
        }

        setContacts(contacts);
    }

	
	public ArrayList<Customer> getCustomers() {
		updateCustomerList();
		// do more, access managedbean
		return customers;
	}
	
	public ArrayList<ContactPerson> getContacts(){
		updateContactPersonList();
		return contacts;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
    
	public void setContacts(ArrayList<ContactPerson> contacts) {
		this.contacts = contacts;
	}
    public void searchAll()
    {
    	customers.clear();
        
        for (Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
    
    public void searchAllContacts()
    {
    	contacts.clear();
        
        for (ContactPerson contact : customerManagedBean.getAllContactPerson())
        {
        	contacts.add(contact);
        }
        
        setCustomers(customers);
    }
    
    public void searchByCustomerId(int customerId) {
    	customers.clear();   	
    	customers.add(customerManagedBean.searchByCustomerId(customerId));
    	    	
    	showCustomer.setCustomerID(customerManagedBean.searchByCustomerId(customerId).getCustomerID());
    	showCustomer.setAddress(customerManagedBean.searchByCustomerId(customerId).getAddress());
    	showCustomer.setName(customerManagedBean.searchByCustomerId(customerId).getName());
    	showCustomer.setPhoneNumber(customerManagedBean.searchByCustomerId(customerId).getPhoneNumber());
    	
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(Integer.toString(customerManagedBean.searchByCustomerId(customerId).getCustomerID())));
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(customerManagedBean.searchByCustomerId(customerId).getAddress()));
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added succesfully"));
		
    }
    public void searchByContactId(int contactId) {
    	contacts.clear();
    	contacts.add(contactPersonManagedBean.searchByContactPersonId(contactId));
    	
    	showContacts.setContactPersonID(contactPersonManagedBean.searchByContactPersonId(contactId).getContactPersonID());
    	showContacts.setFirstName(contactPersonManagedBean.searchByContactPersonId(contactId).getFirstName());
    	showContacts.setLastName(contactPersonManagedBean.searchByContactPersonId(contactId).getLastName());
    	showContacts.setAddress(contactPersonManagedBean.searchByContactPersonId(contactId).getAddress());
    	showContacts.setPhoneNumber(contactPersonManagedBean.searchByContactPersonId(contactId).getAddress());
    	

//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(Integer.toString(contactPersonManagedBean.searchByContactPersonId(contactId).getContactPersonID())));
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(contactPersonManagedBean.searchByContactPersonId(contactId).getAddress()));
//    	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been added succesfully"));
    }
    
    
    
}
