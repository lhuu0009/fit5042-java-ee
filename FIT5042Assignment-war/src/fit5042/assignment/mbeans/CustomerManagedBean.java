package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.ContactPersonRepository;
import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.Customer;




@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
		
	@EJB
    CustomerRepository customerRepository;
	
	@EJB
	ContactPersonRepository contactPersonRepository;
    
    public CustomerManagedBean() {
    }
	
    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public List<ContactPerson> getAllContactPerson(){
    	try {
    		List<ContactPerson> contacts = contactPersonRepository.getAllContactPerson();
    		return contacts;
    	}catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    	
    }
   
    
    public void addCustomer(Customer customer) {
    	try {
    		customerRepository.addCustomer(customer);
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    }

    public void deleteCustomer(int customerId) {
    	try {
    		customerRepository.deleteCustomer(customerId);
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    }
    
    public void modifyCustomer(Customer customer) {
    	try {
    		String newAddress = customer.getAddress();
    		customer.setAddress(newAddress);
    		String newNumber = customer.getPhoneNumber();
    		customer.setPhoneNumber(newNumber);
    		String newName = customer.getName();
    		customer.setName(newName);
    		
    		customerRepository.modifyCustomer(customer);
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer information has been updated succesfully"));
    		
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    }
    
    public Customer searchByCustomerId(int customerId) {
    	try {
    		return customerRepository.searchByCustomerId(customerId);
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    	return null;
    }
    
    public Customer searchByContactPersonId(int contactPersonId) {
    	try {
    		return customerRepository.searchByContactPersonId(contactPersonId);
    	} catch (Exception ex) {
    		Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    	return null;
    }

	public void addCustomer(fit5042.assignment.controllers.Customer localCustomer) {
		// TODO Auto-generated method stub
		Customer customer = convertCustomerToEntity(localCustomer);
		try {
            customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
	}
    
	public Customer convertCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer) {
		Customer customer = new Customer(); // entity
		customer.setCustomerID(localCustomer.getCustomerID());
		
		customer.setName(localCustomer.getName());
		//customer.setLastName(localCustomer.getLastName());
		customer.setPhoneNumber(localCustomer.getPhoneNumber());
		customer.setAddress(localCustomer.getAddress());
		customer.setDate(localCustomer.getDate());
		customer.setEmail(localCustomer.getEmail());
		return customer;
	}
}
