/**
 * 
 */
package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.Customer;
//import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.ContactPerson;

/**
 * @author Administrator
 *
 */
@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext (unitName = "FIT5042Assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}
	
	@Override
	public void addCustomer(Customer customer) throws Exception{
		List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        customer.setCustomerID(customers.get(0).getCustomerID() + 1);
        entityManager.persist(customer);
				
	}

	@Override
	public void deleteCustomer(int customerId) throws Exception{
		// TODO Auto-generated method stub
		
		entityManager.remove(searchByCustomerId(customerId));
		}
		
	@Override
	public void modifyCustomer(Customer customer) throws Exception {
		// TODO Auto-generated method stub
		try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
	}

	@Override
	public Customer searchByCustomerId(int id) throws Exception {
		// TODO Auto-generated method stub
		Customer customer = entityManager.find(Customer.class, id);
        //customer.getTags();
        return customer;
	}

	@Override
	public Customer searchByContactPersonId(int id) throws Exception {
		// TODO Auto-generated method stub
		
        return null;
	}

	

}

