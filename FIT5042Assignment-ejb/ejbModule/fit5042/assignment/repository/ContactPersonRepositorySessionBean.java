package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.Customer;
//import fit5042.assignment.repository.entities.ContactPerson;
import fit5042.assignment.repository.entities.ContactPerson;
@Stateless
public class ContactPersonRepositorySessionBean implements ContactPersonRepository{
	
	@PersistenceContext (unitName = "FIT5042Assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;

	@Override
	public List<ContactPerson> getAllContactPerson() throws Exception {
		// TODO Auto-generated method stub
		return entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public void addContactPerson(ContactPerson contactPerson) throws Exception {
		// TODO Auto-generated method stub
		List<ContactPerson> contactPersons = entityManager.createNamedQuery(ContactPerson.GET_ALL_QUERY_NAME).getResultList();
        contactPerson.setContactPersonID(contactPersons.get(0).getContactPersonID() + 1);
        entityManager.persist(contactPerson);
	}

	@Override
	public void deleteContactPerson(int id) throws Exception {
		// TODO Auto-generated method stub
		entityManager.remove(searchContactPersonByContactPersonId(id));
	}

	@Override
	public void modifyContactPerson(ContactPerson contactPerson) throws Exception {
		// TODO Auto-generated method stub
		try {
            entityManager.merge(contactPerson);
        } catch (Exception ex) {

        }
	}

	@Override
	public ContactPerson searchContactPersonByContactPersonId(int id) throws Exception {
		// TODO Auto-generated method stub
		ContactPerson contactPerson = entityManager.find(ContactPerson.class, id);
        //customer.getTags();
        return contactPerson;
	}

	@Override
	public Set<ContactPerson> searchContactPersonByCustomerId(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
}
