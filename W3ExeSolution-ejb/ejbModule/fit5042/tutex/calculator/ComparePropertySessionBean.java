package fit5042.tutex.calculator;

import java.util.ArrayList;
import javax.ejb.Remote;
import fit5042.tutex.repository.entities.Property;
import fit5042.tutex.repository.constants.CommonInstance;
import javax.ejb.Stateful;
@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	
	private ArrayList<Property> propertyList = new ArrayList<>();
	@Override
	public void addProperty(Property property) {
		propertyList.add(property);
	}
	
	@Override 
	public void removeProperty(Property property) {
		propertyList.remove(property);
	}
	
	@Override
	public int getBestPerRoom() {
		int best = 0;
		double bestPrice = 99999999;
		for (Property p : propertyList) {
			if (p.getPrice()< bestPrice) {
				bestPrice = p.getPrice();
				best = p.getPropertyId();
			}
		}
		return best;
	}
	
}
