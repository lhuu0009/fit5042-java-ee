package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Customer;

@Remote
public interface CustomerRepository {
	
    public List<Customer> getAllCustomers() throws Exception;
    
    public void addCustomer(Customer customer) throws Exception;
     
    public void deleteCustomer(int customerId) throws Exception;
    
    public void modifyCustomer(Customer customer) throws Exception;
    
    public Customer searchByCustomerId(int id) throws Exception;
    
    public Customer searchByContactPersonId(int id) throws Exception;
    
}
