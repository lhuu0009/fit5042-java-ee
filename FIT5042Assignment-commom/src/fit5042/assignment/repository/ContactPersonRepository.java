package fit5042.assignment.repository;

import java.util.List;
import java.util.Set;
import javax.ejb.Remote;

import fit5042.assignment.repository.entities.ContactPerson;

@Remote
public interface ContactPersonRepository {
	
	public List<ContactPerson> getAllContactPerson() throws Exception;
	
	public void addContactPerson(ContactPerson contactPerson) throws Exception;
	
	public void deleteContactPerson(int id) throws Exception;
	
	public void modifyContactPerson(ContactPerson contactPerson) throws Exception;
	
	public ContactPerson searchContactPersonByContactPersonId(int id)throws Exception;
	
	public Set<ContactPerson> searchContactPersonByCustomerId(int id)throws Exception;
}
