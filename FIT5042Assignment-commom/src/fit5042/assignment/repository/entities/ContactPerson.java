package fit5042.assignment.repository.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;



@Entity
@NamedQueries({
	@NamedQuery(name = ContactPerson.GET_ALL_QUERY_NAME, query = "SELECT co FROM ContactPerson co") })
public class ContactPerson implements Serializable{
	
	public static final String GET_ALL_QUERY_NAME =  "ContactPerson.getAll";
	
	private int contactPersonID;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String address;
	private Customer customer;
	private int customerID;
	private String email;
	private String position;
	
	public ContactPerson(){
		
	}
	public ContactPerson(int contactPersonID, String firstName, String lastName, String phoneNumber, String address,
			Customer customer, int customerID, String email, String position) {
		super();
		this.contactPersonID = contactPersonID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.customer = customer;
		this.customerID = customerID;
		this.email = email;
		this.position = position;
	}

	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "contactPerson_id")
	public int getContactPersonID() {
		return contactPersonID;
	}

	public void setContactPersonID(int employeeID) {
		this.contactPersonID = employeeID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@ManyToOne
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ContactPerson other = (ContactPerson) obj;
		if (this.contactPersonID != other.contactPersonID) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "ContactPerson [contactPersonID=" + contactPersonID + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
