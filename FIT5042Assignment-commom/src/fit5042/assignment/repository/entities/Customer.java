
package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c") })
public class Customer implements Serializable{

	public static final String GET_ALL_QUERY_NAME =  "Customer.getAll";
	
	private int customerID;
	private String name;
	private String phoneNumber;
	private String address;
	private Set<ContactPerson> contacts;
	private Date date;
	private String email;
	
	
	//private String industry;
	
	public Customer() {
		
	}
	public Customer(int customerID) {
		this.customerID = customerID;
		this.date = new Date();
	}
	
	public Customer(int customerID, String name, String phoneNumber, String address, String email) {
		super();
		this.customerID = customerID;
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.date = new Date();
		contacts = new HashSet<>();
		this.email = email;
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customer_id")
	public int getCustomerID() {
		return customerID;
	}
	
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@OneToMany(mappedBy = "customer",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<ContactPerson> getContacts() {
		return contacts;
	}
	
	public void setContacts(Set<ContactPerson> contacts) {
		this.contacts = contacts;
	}
	
	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", firstName=" + name  + "]";
	}
//	public String getIndustry() {
//		return industry;
//	}
//	public void setIndustry(String industry) {
//		this.industry = industry;
//	}
//	
		
}